﻿using Dapper;
using DMS.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DMS.DataAccess
{
    public class CompanyProfileRepository : IDisposable,ICompanyProfileRepository
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CompanyProfileRepository));
        public SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            con = new SqlConnection(constr);

        }
        /// <summary>
        /// Add Company Profile Details
        /// </summary>
        /// <param name="companyProfile"></param>
        public bool AddCompanyProfile(CompanyProfile companyProfile)
        {
            bool result = false;

                Logger.Info("M here");
                Connection();
                con.Open();
                var parameters = new
                {
                    iName = companyProfile.Name,
                    iDisplayName = companyProfile.DisplayName,
                    iIsParent = companyProfile.IsParent,
                    iParentId = companyProfile.ParentId,
                    iCompanyLogoPath = companyProfile.CompanyLogoPath,
                    iAddress = companyProfile.Address,
                    iPhone = companyProfile.Phone,
                    iFax = companyProfile.Fax,
                    iWebAddress = companyProfile.WebAddress,
                    iEmail = companyProfile.Email,
                    iCreatorId = companyProfile.Creatorid,
                    iCompanyId = companyProfile.CompanyId
                };
                SqlMapper.Execute(con, "CompanyProfile_Create", parameters, commandType: CommandType.StoredProcedure);
                result = true;
            
                con.Close();
            
            return result;
        }
        /// <summary>
        /// Update company profile details
        /// </summary>
        /// <param name="companyProfile"></param>
        public void UpdateCompanyProfile(CompanyProfile companyProfile)
        {
            
                Connection();
                con.Open();
                var parameters = new
                {
                    iId = companyProfile.Id,
                    iName = companyProfile.Name,
                    iDisplayName = companyProfile.DisplayName,
                    iIsParent = companyProfile.IsParent,
                    iParentId = companyProfile.ParentId,
                    iCompanyLogoPath = companyProfile.CompanyLogoPath,
                    iAddress = companyProfile.Address,
                    iPhone = companyProfile.Phone,
                    iFax = companyProfile.Fax,
                    iWebAddress = companyProfile.WebAddress,
                    iEmail = companyProfile.Email,
                    iUpdatorId = companyProfile.Creatorid,
                    iCompanyId = companyProfile.CompanyId
                };
                SqlMapper.Execute(con, "CompanyProfile_Update", parameters, commandType: CommandType.StoredProcedure);
           
                con.Close();
           
        }
        /// <summary>
        /// Get company details by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CompanyProfile GetCompanyProfileById(int Id)
        {
            IList<CompanyProfile> EmpList = new List<CompanyProfile>();
            
                Connection();
                con.Open();
                var param = new
                {
                    iId = Id
                };
                EmpList = SqlMapper.Query<CompanyProfile>(
                                  con, "CompanyProfile_GetById", param, commandType: CommandType.StoredProcedure).ToList();
                con.Close();

            
                con.Close();
            
            return EmpList.FirstOrDefault();
        }
        /// <summary>
        /// Get all company profiles
        /// </summary>
        /// <returns></returns>
        public List<CompanyProfile> GetAllCompanyProfiles()
        {
            List<CompanyProfile> customers = new List<CompanyProfile>();          
                Connection();
                con.Open();
                customers = SqlMapper.Query<CompanyProfile>(con, "CompanyProfile_GetAll").ToList();           
                con.Close();            
            return customers;
        }

        public void DeleteCompanyProfile(int companyProfileId)
        {          
                Connection();
                con.Open();
                var param = new
                {
                    iId = companyProfileId,
                };

                SqlMapper.Execute(con, "CompanyProfile_DeleteById", param, commandType: CommandType.StoredProcedure);            
                con.Close();            

        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~CompanyProfileRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

    public interface ICompanyProfileRepository
    {
        bool AddCompanyProfile(CompanyProfile companyProfile);
        void UpdateCompanyProfile(CompanyProfile companyProfile);
        CompanyProfile GetCompanyProfileById(int Id);
        List<CompanyProfile> GetAllCompanyProfiles();
        void DeleteCompanyProfile(int companyProfileId);
    }
}
