﻿using Dapper;
using DMS.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DMS.DataAccess
{
    public class DepartmentRepository : IDisposable
    {


        private static readonly ILog Logger = LogManager.GetLogger(typeof(DepartmentRepository));
        public SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            con = new SqlConnection(constr);

        }
        /// <summary>
        /// Add Department Details
        /// </summary>
        /// <param name="department"></param>
        public void AddDepartment(Department department)
        {

            Connection();
            con.Open();
            var parameters = new
            {

                iName = department.Name,
                iCompanyId = department.CompanyId,
                iCreatorId = department.CreatorId

            };
            SqlMapper.Execute(con, "Department_Create", parameters, commandType: CommandType.StoredProcedure);
            con.Close();

        }

        /// <summary>
        /// Update Department details
        /// </summary>
        /// <param name="Department"></param>
        public bool UpdateDepartment(Department department)
        {
            bool result = false;


            Connection();
            con.Open();
            var parameters = new
            {
                iId = department.Id,
                iName = department.Name,
                iCompanyId = department.CompanyId,
                iUpdatorId = department.UpdatorId

            };
            SqlMapper.Execute(con, "Department_Update", parameters, commandType: CommandType.StoredProcedure);
            result = true;
            con.Close();
            return result;
        }

        /// <summary>
        /// Get Department details by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Department GetDepartmentById(int Id)
        {
            List<Department> EmpList = new List<Department>();
            try
            {
                Connection();
                con.Open();
                var param = new
                {
                    iId = Id
                };
                EmpList = SqlMapper.Query<Department>(
                                  con, "Department_GetById", param, commandType: CommandType.StoredProcedure).ToList();
                con.Close();

            }
            catch (Exception ex)
            {

                Logger.Error("Error occured while Get Department details By Id:" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return EmpList.FirstOrDefault();
        }

        /// <summary>
        /// Get Department details by companyid
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public List<Department> GetDepartmentByCompanyId(int Id)
        {
            List<Department> EmpList = new List<Department>();
            try
            {
                Connection();
                con.Open();
                var param = new
                {
                    iCompanyId = Id
                };
                EmpList = SqlMapper.Query<Department>(
                                  con, "Department_GetBycompanyId", param, commandType: CommandType.StoredProcedure).ToList();
                con.Close();

            }
            catch (Exception ex)
            {

                Logger.Error("Error occured while Get Department details By ompanyId:" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return EmpList;
        }

        /// <summary>
        /// Get all Department profiles
        /// </summary>
        /// <returns></returns>
        public List<Department> GetAllDepartment()
        {
            List<Department> customers = new List<Department>();
            try
            {
                Connection();
                con.Open();
                customers = SqlMapper.Query<Department>(con, "Department" + "_GetAll").ToList();

            }
            catch (Exception ex)
            {

                Logger.Error("Error occured while Get All Company profile details:" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return customers;
        }

        /// <summary>
        /// Get Department delete by companyid
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public void DeleteDepartment(int Id)
        {


            Connection();
            con.Open();

            var param = new
            {
                iId = Id,

            };

            SqlMapper.Execute(con, "Department_DeleteById", param, commandType: CommandType.StoredProcedure);

            con.Close();

        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }



        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
