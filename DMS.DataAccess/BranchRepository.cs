﻿
using Dapper;
using DMS.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DMS.DataAccess
{
    public class BranchRepository : IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(BranchRepository));
        public SqlConnection con;
        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            con = new SqlConnection(constr);

        }
        /// <summary>
        /// Add Branch Details
        /// </summary>
        /// <param name="Branch"></param>
        public void AddBranch(Branch branch)
        {

            connection();
            con.Open();
            var parameters = new
            {

                iName = branch.Name,
                iDisplayName = branch.DisplayName,
                iAddress = branch.Address,
                iPhone = branch.Phone,
                iFax = branch.Fax,
                iWebAddress = branch.WebAddress,
                iEmail = branch.Email,
                iCreatorId = branch.Creatorid,
                iCompanyId = branch.CompanyId,
                iBranchcode = branch.Branchcode

            };
            SqlMapper.Execute(con, "Branch_Create", parameters, commandType: CommandType.StoredProcedure);
            con.Close();

        }

        /// <summary>
        /// Update Branch details
        /// </summary>
        /// <param name="Branch"></param>
        public bool UpdateBranch(Branch branch)
        {
            bool result = false;


            connection();
            con.Open();
            var parameters = new
            {
                iId = branch.Id,
                iName = branch.Name,
                iDisplayName = branch.DisplayName,
                iAddress = branch.Address,
                iPhone = branch.Phone,
                iFax = branch.Fax,
                iWebAddress = branch.WebAddress,
                iEmail = branch.Email,
                iUpdatorId = branch.UpdatorId,
                iCompanyId = branch.CompanyId,
                iBranchcode = branch.Branchcode
            };
            SqlMapper.Execute(con, "Branch_Update", parameters, commandType: CommandType.StoredProcedure);
            result = true;
            con.Close();
            return result;
        }
        /// <summary>
        /// Get Branch details by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Branch GetBranchById(int Id)
        {
            IList<Branch> EmpList = new List<Branch>();

            connection();
            con.Open();
            var param = new
            {
                iId = Id
            };
            EmpList = SqlMapper.Query<Branch>(
                              con, "Branch_GetById", param, commandType: CommandType.StoredProcedure).ToList();
            con.Close();


            return EmpList.FirstOrDefault()
;
        }
        /// <summary>
        /// Get Branch details by companyid
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Branch GetUserByCompanyId(int Id)
        {
            IList<Branch> EmpList = new List<Branch>();
            try
            {
                connection();
                con.Open();
                var param = new
                {
                    iCompanyId = Id
                };
                EmpList = SqlMapper.Query<Branch>(
                                  con, "Branch_GetBycompanyId", param, commandType: CommandType.StoredProcedure).ToList();
                con.Close();

            }
            catch (Exception ex)
            {

                Logger.Error("Error occured while Get Branch details By ompanyId:" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return EmpList.FirstOrDefault()
;
        }
        /// <summary>
        /// Get all Users
        /// </summary>
        /// <returns></returns>
        public List<Branch> GetAllBranch()
        {
            List<Branch> customers = new List<Branch>();


            connection();
            con.Open();
            customers = SqlMapper.Query<Branch>(con, "Branch_GetAll").ToList();

            con.Close();
            return customers;
        }

        public void DeleteBranch(int Id)
        {


                connection();
                con.Open();

                var param = new
                {
                    iId = Id,

                };

                SqlMapper.Execute(con, "Branch_DeleteById", param, commandType: CommandType.StoredProcedure);

            con.Close();

        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }



        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
