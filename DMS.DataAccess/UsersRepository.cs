﻿using Dapper;
using DMS.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace DMS.DataAccess
{
    public class UsersRepository : IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UsersRepository));
        public SqlConnection con;
        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            con = new SqlConnection(constr);

        }
        /// <summary>
        /// Add Users Details
        /// </summary>
        /// <param name="Users"></param>
        public void AddUsers(Users users)
        {
            try
            {
                connection();
                con.Open();
                var parameters = new
                {

                    iLoginName = users.LoginName,
                    iLoginPassword = users.LoginPassword,
                    iCompanyId = users.CompanyId,
                    iBranchId = users.BranchId,
                    iUserName=users.UserName,
                    iPhone=users.Phone,
                    iEmail=users.Email,
                    iRole=users.Role,
                    iCreatorId=users.CreatorId

                };
                SqlMapper.Execute(con, "User_Create", parameters, commandType: CommandType.StoredProcedure);

            }
            catch (Exception e)
            {

                Logger.Error("Error occured while Adding User details:" + e.Message);
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Update Users details
        /// </summary>
        /// <param name="Users"></param>
        public void UpdateUsers(Users users)
        {
            try
            {
                connection();
                con.Open();
                var parameters = new
                {
                    iId = users.Id,
                    iLoginName = users.LoginName,
                    iLoginPassword = users.LoginPassword,
                    iCompanyId = users.CompanyId,
                    iBranchId = users.BranchId,                    
                    iUserName = users.UserName,
                    iPhone = users.Phone,
                    iEmail = users.Email,
                    iRole = users.Role,
                    iUpdatorId =users.UpdatorId
                };
                SqlMapper.Execute(con, "User_Update", parameters, commandType: CommandType.StoredProcedure);

            }
            catch (Exception e)
            {

                Logger.Error("Error occured while updating Users details:" + e.Message);
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Get User details by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Users GetUserById(int Id)
        {
            IList<Users> EmpList = new List<Users>();
            try
            {
                connection();
                con.Open();
                var param = new
                {
                    iId = Id
                };
                EmpList = SqlMapper.Query<Users>(
                                  con, "User_GetById", param, commandType: CommandType.StoredProcedure).ToList();
                con.Close();

            }
            catch (Exception ex)
            {

                Logger.Error("Error occured while Get Users details By Id:" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return EmpList.FirstOrDefault()
;
        }

        public Users GetUserByUserName(string userName)
        {
            IList<Users> EmpList = new List<Users>();
            try
            {
                connection();
                con.Open();
                var param = new
                {
                    iName = userName
                };
                EmpList = SqlMapper.Query<Users>(con, "User_GetByName", param, commandType: CommandType.StoredProcedure).ToList();
                con.Close();

            }
            catch (Exception ex)
            {

                Logger.Error("Error occured while Get Users details By Id:" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return EmpList.FirstOrDefault()
;
        }
        /// <summary>
        /// Get all Users
        /// </summary>
        /// <returns></returns>
        public List<Users> GetAllUsers()
        {
            List<Users> customers = new List<Users>();
            try
            {
                connection();
                con.Open();
                customers = SqlMapper.Query<Users>(con, "User_GetAll").ToList();

            }
            catch (Exception ex)
            {

                Logger.Error("Error occured while Get All User details:" + ex.Message);
            }
            finally
            {
                con.Close();
            }
            return customers;
        }

        public void DeleteUser(int UserId)
        {


            try
            {
                connection();
                con.Open();

                var param = new
                {
                    iId = UserId,

                };

                SqlMapper.Execute(con, "User_DeleteById", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("Error occured while delete User By Id:{0} Exception:", UserId, ex.Message));
            }
            finally
            {
                con.Close();
            }

        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }



        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
