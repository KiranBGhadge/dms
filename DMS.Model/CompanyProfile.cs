﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DMS.Model
{
    public class CompanyProfile
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("DisplayName")]
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        [JsonProperty("IsParent")]
        public bool IsParent { get; set; }

        [JsonProperty("ParentId")]
        public long ParentId { get; set; }

        [Display(Name = "Company Logo Path")]
        [JsonProperty("CompanyLogoPath")]
        public string CompanyLogoPath { get; set; }

        [JsonProperty("CompanyId")]
        public string CompanyId { get; set; }

        [JsonProperty("Address")]
        public string Address { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Fax")]
        public string Fax { get; set; }

        [JsonProperty("WebAddress")]
        [Display(Name = "Web Address")]
        public string WebAddress { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Creatorid")]
        public long Creatorid { get; set; }

        [JsonProperty("UpdatorId")]
        public long UpdatorId { get; set; }

    }
}
