﻿using Newtonsoft.Json;
using System;

namespace DMS.Model
{
   public class Branch
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("DisplayName")]
        public string DisplayName { get; set; }
        [JsonProperty("Branchcode")]
        public string Branchcode { get; set; }
        [JsonProperty("CompanyId")]
        public int CompanyId { get; set; }
        [JsonProperty("Address")]
        public string Address { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
        [JsonProperty("Fax")]
        public string Fax { get; set; }
        [JsonProperty("WebAssress")]
        public string WebAddress { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("Creatorid")]
        public int Creatorid { get; set; }
        [JsonProperty("CreateDate")]
        public DateTime CreateDate { get; set; }
        [JsonProperty("UpdatorId")]
        public int UpdatorId { get; set; }
        [JsonProperty("UpdateDate")]
        public DateTime UpdateDate { get; set; }

    }
}
