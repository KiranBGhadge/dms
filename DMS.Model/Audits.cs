﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    public class Audits
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime ActivityDate { get; set; }
        public string TableName { get; set; }
        public int RecordId { get; set; }
        public int AuditType { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string ChangedColumn { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
