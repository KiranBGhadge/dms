﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace DMS.Model
{

    public class Users
    {
        private user_info userinfo = new user_info();

        public int Id { get; set; }
        [Display(Name = "Login Name")]
        public string LoginName { get; set; }

        public string LoginPassword { get; set; }

        [Display(Name = "Company")]
        public int CompanyId { get; set; }

        [Display(Name = "Branch")]
        public int BranchId { get; set; }

        public string UserName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public DateTime LastLoginDate { get; set; }

        public string LastLoginIP { get; set; }

        public int LastAlertId { get; set; }

        public int LastEventId { get; set; }

        public DateTime LastPasswordChanged { get; set; }

        public int IsReset { get; set; }

        public int IsActive { get; set; }

        public string LoginType { get; set; }

        public int CreatorId { get; set; }

        public DateTime CreateDate { get; set; }

        public int UpdatorId { get; set; }

        public DateTime UpdateDate { get; set; }

        public int SessionId { get; set; }

        public string SessionTimeout { get; set; }

        public int IsProperLoggedOut { get; set; }

        public DateTime LastLoggedOutDate { get; set; }

        public string AccessRole { get; set; }

        public user_info userinformation
        {
            get { return userinfo; }
            set { userinfo = value; }
        }
    }
}
