﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    public class Docattachedlob
    {
        public int ID { get; set; }
        public int AttachmentId { get; set; }
        public string Attachment { get; set; }
        public string MimeType { get; set; }
        public string GUID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }


    }
}
