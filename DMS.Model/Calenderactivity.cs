﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    public class Calenderactivity
    {
        public int Id { get; set; }
        public int CompanyID { get; set; }
        public int ActivityType { get; set; }
        public string RevisedStartDate { get; set; }
        public DateTime RevisedEndDate { get; set; }
        public int ProfileID { get; set; }
        public string ActivityName { get; set; }
        public string ActivityDetails { get; set; }
        public DateTime ScheduledStartDate { get; set; }
        public DateTime ScheduledENDDate { get; set; }
        public DateTime ActualEndDate { get; set; }
        public string Remarks { get; set; }
        public int Status { get; set; }
        public int Responsibility { get; set; }
        public string Reminder { get; set; }
        public string RecurrenceRule { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}
