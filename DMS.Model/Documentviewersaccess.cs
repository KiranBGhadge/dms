﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    public class Documentviewersaccess
    {
        public int Id { get; set; }
        public int ViewAccID { get; set; }
        public int DocViewId { get; set; }
        public int AccessID { get; set; }

    }
}
