﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    public class Documentviewers
    {
        public int DocViewId { get; set; }
        public int DocID { get; set; }
        public int IsTeam { get; set; }
        public int UserID { get; set; }
        public int CreatedBy { get; set; }

    }
}
