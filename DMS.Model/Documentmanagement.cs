﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    class Documentmanagement
    {
        public int DocId { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public int DepartmentID { get; set; }
        public int SubFunctionID { get; set; }
        public string DocCode { get; set; }
        public string DocName { get; set; }
        public string DocDescription { get; set; }
        public int DocStatus { get; set; }
        public string Version { get; set; }
        public int OwnerID { get; set; }
        public int AltOwnerID { get; set; }
        public int ApproverID { get; set; }
        public DateTime EffectiveDateDocLevel { get; set; }
        public DateTime ReviewDate { get; set; }
        public DateTime LastReviewDate { get; set; }
        public int IsActive { get; set; }
        public int CreatorId { get; set; }
        public DateTime CreateDate { get; set; }
        public int UpdatorId { get; set; }
        public DateTime UpdateDate { get; set; }
        public int SaveAsType { get; set; }
        public int IsEffective { get; set; }
        public string VersionDetails { get; set; }
        public int DocTypeID { get; set; }

    }
}
