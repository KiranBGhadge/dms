﻿
namespace DMS.Model
{
    public class UserRole
    {
        public int Id { get; set; }

        public string Role { get; set; }

        public string Access { get; set; }
    }
}
