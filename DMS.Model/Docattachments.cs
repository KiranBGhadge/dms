﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    public class Docattachments
    {
        public int Id { get; set; }
        public int AttachmentId { get; set; }
        public int EntityId { get; set; }
        public int RecordID { get; set; }
        public string AttachmentName { get; set; }
        public string Description { get; set; }
        public string CreatorID { get; set; }
        public string CreateDate { get; set; }
        public string UpdatorID { get; set; }
        public DateTime UpdateDate { get; set; }
        public string GUID { get; set; }
        public int FileSize { get; set; }

        public int Filecount { get; set; }

    }
}
