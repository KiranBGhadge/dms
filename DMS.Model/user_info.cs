﻿
using System;

namespace DMS.Model
{
    public class user_info
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string AlertMode { get; set; }

        public int IsActive { get; set; }

        public int CreatorId { get; set; }

        public DateTime CreateDate { get; set; }

        public int UpdatorId { get; set; }

        public DateTime UpdateDate { get; set; }

    }
}
