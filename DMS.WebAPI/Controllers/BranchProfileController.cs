﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DMS.DataAccess;
using DMS.Model;
namespace DMS.WebAPI.Controllers
{
    public class BranchProfileController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns>All branches</returns>
        [HttpGet]
       
        public HttpResponseMessage GetAll()
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (BranchRepository branchRepository = new BranchRepository())
                {
                    var allBranches = branchRepository.GetAllBranch().ToArray();
                    if (allBranches == null)
                        responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found");
                    else
                        responseMessage = Request.CreateResponse(HttpStatusCode.OK, allBranches);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (BranchRepository branchRepository = new BranchRepository())
                {
                    Branch branch = branchRepository.GetBranchById(id);
                    var message = string.Format("Branch with id = {0} not found", id);
                    if (branch == null)
                        responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
                    else responseMessage = Request.CreateResponse(HttpStatusCode.OK, branch);
                }
            }
            catch (Exception ex)
            {

                responseMessage=Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex.Message);
            }
            return responseMessage;
        }
        // POST: api/CompanyProfile
        [HttpPost]
        public HttpResponseMessage AddBranch([FromBody] Branch branch)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {

                using (BranchRepository repository = new BranchRepository())
                {
                    repository.AddBranch(branch);
                }
                httpResponse = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {

                httpResponse = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return httpResponse;
        }

        [HttpPost]
        public HttpResponseMessage UpdateBranchProfile([FromBody]Branch branch)
        {
            bool rs = false;
            HttpResponseMessage httpResponseMessage=new HttpResponseMessage();
            try
            {
                using (BranchRepository repository = new BranchRepository())
                {
                    rs = repository.UpdateBranch(branch);

                }
                if (rs)
                    httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
                
            }
            catch (Exception ex)
            {

                httpResponseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return httpResponseMessage;
        }

        [HttpGet]
        public HttpResponseMessage Delete([FromUri] int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (BranchRepository repository = new BranchRepository())
                {
                    repository.DeleteBranch(id);
                    responseMessage = Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }

    }
}
