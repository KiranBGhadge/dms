﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DMS.DataAccess;
using DMS.Model;

namespace DMS.WebAPI.Controllers
{
    public class DepartmentController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns>All branches</returns>
        [HttpGet]

        public HttpResponseMessage GetAll()
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (DepartmentRepository departmentRepository = new DepartmentRepository())
                {
                    var alldepartments = departmentRepository.GetAllDepartment().ToArray();
                    if (alldepartments == null)
                        responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found");
                    else
                        responseMessage = Request.CreateResponse(HttpStatusCode.OK, alldepartments);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (DepartmentRepository departmentRepository = new DepartmentRepository())
                {
                    Department department = departmentRepository.GetDepartmentById(id);
                    var message = string.Format("Departments with id = {0} not found", id);
                    if (department == null)
                        responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
                    else responseMessage = Request.CreateResponse(HttpStatusCode.OK, department);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }

        public HttpResponseMessage GetByCompany(int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (DepartmentRepository departmentRepository = new DepartmentRepository())
                {
                    Department department = departmentRepository.GetDepartmentById(id);
                    var message = string.Format("Departments with id = {0} not found", id);
                    if (department == null)
                        responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
                    else responseMessage = Request.CreateResponse(HttpStatusCode.OK, department);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }
        // POST: api/Department
        [HttpPost]
        public HttpResponseMessage AddDepartment([FromBody] Department department)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {

                using (DepartmentRepository repository = new DepartmentRepository())
                {
                    repository.AddDepartment(department);
                }
                httpResponse = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {

                httpResponse = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return httpResponse;
        }

        [HttpPost]
        public HttpResponseMessage UpdateDepartment([FromBody]Department department)
        {
            bool rs = false;
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            try
            {
                using (DepartmentRepository repository = new DepartmentRepository())
                {
                    rs = repository.UpdateDepartment(department);

                }
                if (rs)
                    httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);

            }
            catch (Exception ex)
            {

                httpResponseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return httpResponseMessage;
        }

        [HttpGet]
        public HttpResponseMessage Delete([FromUri] int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (DepartmentRepository repository = new DepartmentRepository())
                {
                    repository.DeleteDepartment(id);
                    responseMessage = Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }
    }
}
