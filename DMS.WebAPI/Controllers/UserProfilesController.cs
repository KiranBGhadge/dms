﻿using DMS.DataAccess;
using DMS.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.WebAPI.Controllers
{
    public class UserProfilesController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (UsersRepository usersRepository = new UsersRepository())
                {
                    var allUsers = usersRepository.GetAllUsers().ToArray();
                    if (allUsers == null)
                    {
                        responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found");
                    }
                    else
                    {
                        responseMessage = Request.CreateResponse(HttpStatusCode.OK, allUsers);
                    }
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (UsersRepository usersRepository = new UsersRepository())
                {
                    Users users = usersRepository.GetUserById(id);
                    var message = string.Format("User with id = {0} not found", id);
                    if (users == null)
                    {
                        responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
                    }
                    else
                    {
                        responseMessage = Request.CreateResponse(HttpStatusCode.OK, users);
                    }
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }

        [HttpGet]
        public HttpResponseMessage GetByUserName(string id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (UsersRepository usersRepository = new UsersRepository())
                {
                    Users users = usersRepository.GetUserByUserName(id);
                    var message = string.Format("User with Name = {0} not found", id);
                    if (users == null)
                    {
                        responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
                    }
                    else
                    {
                        responseMessage = Request.CreateResponse(HttpStatusCode.OK, users);
                    }
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }

        [HttpPost]
        public HttpResponseMessage UpdateUserProfile([FromBody]Users users)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            try
            {
                using (UsersRepository repository = new UsersRepository())
                {
                    repository.UpdateUsers(users);

                }

                httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);

            }
            catch (Exception ex)
            {

                httpResponseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return httpResponseMessage;
        }
       [HttpPost]
        public HttpResponseMessage AddUser([FromBody] Users user)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                //user = new Users();
                //user.LoginName = "PGokul.Pailvan";
                //user.LoginPassword = "Raj@2020";
                //user.CompanyId = "1";
                //user.BranchId = 11;
                //user.UserName = "Pandurang Pailvan";
                //user.Email = "panduranggpailvan@gmail.com";
                //user.Phone = "1234567890";
                //user.CreatorId = 1;

                using (UsersRepository repository = new UsersRepository())
                {
                    repository.AddUsers(user);
                }
                httpResponse = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {

                httpResponse = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return httpResponse;
        }

        [HttpGet]
        public HttpResponseMessage Delete([FromUri] int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (UsersRepository repository = new UsersRepository())
                {
                    repository.DeleteUser(id);
                    responseMessage = Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }
    }
}
