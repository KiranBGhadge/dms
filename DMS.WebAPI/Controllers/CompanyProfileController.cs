﻿using DMS.DataAccess;
using DMS.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
namespace DMS.WebAPI.Controllers
{

    public class CompanyProfileController : ApiController
    {

        // GET: api/CompanyProfile
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            try
            {
                using (CompanyProfileRepository companyProfileRepository = new CompanyProfileRepository())
                {
                    var companyProfiles = companyProfileRepository.GetAllCompanyProfiles().ToArray();
                    if (companyProfiles == null && companyProfiles.Length == 0)
                        httpResponseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, "No data found");
                    else
                        httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, companyProfiles);

                }
            }
            catch (Exception ex)
            {
                httpResponseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return httpResponseMessage;
        }

        // GET: api/CompanyProfile/5
        public HttpResponseMessage Get(int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (CompanyProfileRepository companyProfileRepository = new CompanyProfileRepository())
                {
                    CompanyProfile companyProfile = companyProfileRepository.GetCompanyProfileById(id);
                    if (companyProfile == null)
                        responseMessage = Request.CreateResponse(HttpStatusCode.NotFound, "No company details found for Id:" + id);
                    else
                        responseMessage = Request.CreateResponse(HttpStatusCode.OK, companyProfile);
                }
            }
            catch (Exception ex)
            {
                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return responseMessage;
        }

        // POST: api/CompanyProfile
        public HttpResponseMessage AddCompany([FromBody] CompanyProfile companyProfile)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            try
            {
                using (CompanyProfileRepository repository = new CompanyProfileRepository())
                {
                    repository.AddCompanyProfile(companyProfile);
                    responseMessage = Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }

        // PUT: api/CompanyProfile/5
        public HttpResponseMessage UpdateCompanyProfile([FromBody]CompanyProfile companyProfile)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();

            try
            {
                using (CompanyProfileRepository repository = new CompanyProfileRepository())
                {
                    repository.UpdateCompanyProfile(companyProfile);
                    responseMessage = Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }

        // DELETE: api/CompanyProfile/5
        [HttpGet]
        public HttpResponseMessage Delete([FromUri] int id)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();

            try
            {
                using (CompanyProfileRepository companyProfileRepository = new CompanyProfileRepository())
                {
                    companyProfileRepository.DeleteCompanyProfile(id);
                    Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {

                responseMessage = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return responseMessage;
        }
    }
}
