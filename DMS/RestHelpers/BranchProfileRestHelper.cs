﻿using System;
using System.Collections.Generic;
using System.Configuration;
using DMS.Model;
using Newtonsoft.Json;
using RestSharp;

namespace DMS.RestHelpers
{
    public class BranchProfileRestHelper:IDisposable
    {

        public IRestResponse GetBranchProfiles()
        {
           
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "BranchProfile/GetAll");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            var response = client.Get(request);            

            return response;
        }
        public IRestResponse AddBranchProfile(Branch branch)
        {
           
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "BranchProfile/AddBranch");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            string sJson = JsonConvert.SerializeObject(branch);
            request.AddJsonBody(sJson);
            var response = client.Post(request);           

            return response;
        }
        public IRestResponse UpdateBranchProfile(Branch branch)
        {
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "BranchProfile/UpdateBranchProfile");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            string sJson = JsonConvert.SerializeObject(branch);
            request.AddJsonBody(sJson);
            var response = client.Post(request);
            
            return response;
        }

        public Branch GetBranchProfileById(int branchId)
        {
            Branch branchProfile = null;
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "BranchProfile/Get/" + branchId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                branchProfile = JsonConvert.DeserializeObject<Branch>(response.Content);
            }
            return branchProfile;
        }
        public IRestResponse DeleteBranchProfileById(int branchId)
        {
           
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "BranchProfile/Delete/" + branchId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            
            return response;
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BranchProfileRestHelper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}