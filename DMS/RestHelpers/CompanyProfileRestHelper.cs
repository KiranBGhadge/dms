﻿using DMS.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace DMS.RestHelpers
{
    public class CompanyProfileRestHelper : IDisposable
    {
        public IRestResponse GetCompanyProfiles()
        {
            IList<CompanyProfile> companyProfiles = new List<CompanyProfile>();
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "CompanyProfile/GetAll");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            var response = client.Get(request);            

            return response;
        }
        public IRestResponse AddCompanyProfile(CompanyProfile companyProfile)
        {           
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "CompanyProfile/AddCompany");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            string sJson = JsonConvert.SerializeObject(companyProfile);
            request.AddJsonBody(sJson);

            IRestResponse response = client.Post(request);
            
            return response;
        }
        public IRestResponse UpdateCompanyProfile(CompanyProfile companyProfile)
        {
            
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "CompanyProfile/UpdateCompanyProfile");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            string sJson = JsonConvert.SerializeObject(companyProfile);
            request.AddJsonBody(sJson);
            var response = client.Post(request);           

            return response;
        }
        public CompanyProfile GetCompanyProfileById(int companyId)
        {
            CompanyProfile companyProfile = null;
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "CompanyProfile/Get/" + companyId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                companyProfile = JsonConvert.DeserializeObject<CompanyProfile>(response.Content);
            }
            return companyProfile;
        }
        public IRestResponse DeleteCompanyProfileById(int companyId)
        {
           
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "CompanyProfile/Delete/" + companyId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            
            return response;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~CompanyProfileRestHelper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class CompanyProfileJsonHelper
    {
        public IList<CompanyProfile> CompanyProfiles { get; set; }
    }
}