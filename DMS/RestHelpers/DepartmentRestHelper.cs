﻿using System.Collections.Generic;
using System.Configuration;
using DMS.Model;
using Newtonsoft.Json;
using RestSharp;
using System;

namespace DMS.RestHelpers
{
    public class DepartmentRestHelper:IDisposable
    {
        public IRestResponse GetDepartments()
        {
            
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "Department/GetAll");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            var response = client.Get(request);
            
            return response;
        }
        public IRestResponse AddDepartment(Department department)
        {
           
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "Department/AddDepartment");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            string sJson = JsonConvert.SerializeObject(department);
            request.AddJsonBody(sJson);
            var response = client.Post(request);
            

            return response;
        }
        public IRestResponse UpdateDepartment(Department department)
        {
            
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "Department/UpdateDepartment");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            string sJson = JsonConvert.SerializeObject(department);
            request.AddJsonBody(sJson);
            var response = client.Post(request);
            

            return response;
        }
        public Department GetDepartmentById(int Id)
        {
            Department departmente = null;
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "Department/Get/" + Id);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                departmente = JsonConvert.DeserializeObject<Department>(response.Content);
            }
            return departmente;
        }
        public IRestResponse GetDepartmentByCompanyId(int companyId)
        {
            
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "Department/Get/" + companyId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            //if (response.StatusCode == System.Net.HttpStatusCode.OK)
            //{
            //    departmente = JsonConvert.DeserializeObject<Department>(response.Content);
            //}
            return response;
        }
        public IRestResponse DeleteDepartmentById(int departmentId)
        {
            
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "Department/Delete/" + departmentId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            var response = client.Get(request);        
            
            return response;
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BranchProfileRestHelper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}