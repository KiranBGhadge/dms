﻿using System;
using System.Configuration;
using DMS.Model;
using Newtonsoft.Json;
using RestSharp;

namespace DMS.RestHelpers
{
    public class UserRestHelper:IDisposable
    {
        public IRestResponse GetAllUserProfiles()
        {
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "UserProfiles/GetAll");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            var response = client.Get(request);

            return response;
        }

        public IRestResponse AddUser(Users users)
        {

            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "UserProfiles/AddUser");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            string sJson = JsonConvert.SerializeObject(users);
            request.AddJsonBody(sJson);
            var response = client.Post(request);

            return response;
        }

        public IRestResponse UpdateUserProfile(Users users)
        {
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "UserProfiles/UpdateUserProfile");
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");
            string sJson = JsonConvert.SerializeObject(users);
            request.AddJsonBody(sJson);
            var response = client.Post(request);

            return response;
        }


        public Users GetUserProfileById(int userId)
        {
            Users userProfile = null;
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "UserProfiles/Get/" + userId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                userProfile = JsonConvert.DeserializeObject<Users>(response.Content);
            }
            return userProfile;
        }
        public Users GetUserProfileByName(string userId)
        {
            Users userProfile = null;
            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "UserProfiles/GetByUserName/" + userId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                userProfile = JsonConvert.DeserializeObject<Users>(response.Content);
            }
            return userProfile;
        }
        public IRestResponse DeleteUserProfileById(int userId)
        {

            string url = ConfigurationManager.AppSettings["dmsRestURL"].ToString();
            var client = new RestClient(url + "UserProfiles/Delete/" + userId);
            var request = new RestRequest();
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);

            return response;
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UserRestHelper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}