﻿using System.Web;
using System.Web.Optimization;

namespace DMS
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        //"~/Scripts/jquery-{version}.js",
                        "~/assets/js/core/jquery.min.js",
                        "~/assets/js/core/bootstrap.min.js",
                        "~/assets/js/core/jquery.slimscroll.min.js",
                        "~/assets/js/core/jquery.scrollLock.min.js",
                        "~/assets/js/core/jquery.placeholder.min.js",
                        "~/assets/js/app.js",
                        "~/assets/js/app-custom.js",
                        "~/assets/js/plugins/slick/slick.min.js",
                        "~/assets/js/plugins/chartjs/Chart.min.js",
                        "~/assets/js/plugins/flot/jquery.flot.min.js",
                        "~/assets/js/plugins/flot/jquery.flot.pie.min.js",
                        "~/assets/js/plugins/flot/jquery.flot.stack.min.js",
                        "~/assets/js/plugins/flot/jquery.flot.resize.min.js",
                       // "~/assets/js/pages/index.js",
                        "~/assets/js/plugins/datatables/jquery.dataTables.min.js",
                        "~/Scripts/selectize/js/standalone/selectize.js",
                         "~/Scripts/ExporttoExcel/jquery.table2excel.min.js"
                        // "~/assets/js/pages/base_tables_datatables.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                .Include( //"~/Scripts/bootstrap.js"
                      
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      //"~/Content/bootstrap.css",
                      //"~/Content/site.css"
                      "~/assets/js/plugins/slick/slick.min.css",
                      "~/assets/js/plugins/slick/slick-theme.min.css",
                      "~/assets/css/font-awesome.css",
                      "~/assets/css/ionicons.css",
                      "~/assets/css/bootstrap.css",
                      "~/assets/css/app.css",
                      "~/assets/css/app-custom.css",
                      "~/assets/js/plugins/datatables/jquery.dataTables.min.css",
                       "~/Scripts/selectize/css/selectize.css",
                      "~/Scripts/selectize/css/selectize.bootstrap3.css"
                      ));
        }
    }
}
