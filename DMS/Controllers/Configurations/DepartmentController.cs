﻿using AutoMapper;
using DMS.Extensions;
using DMS.Model;
using DMS.Models;
using DMS.RestHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace DMS.Controllers.Configurations
{
    public class DepartmentController : Controller
    {
        // GET: Department
        public ActionResult Index()
        {
            using (DepartmentRestHelper restHelper = new DepartmentRestHelper())
            {
                IList<Department> departments = new List<Department>();
                var response = restHelper.GetDepartments();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    departments = JsonConvert.DeserializeObject<List<Department>>(response.Content);
                    return View(departments);
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    return View(departments);
                }
            }

        }


        // GET: Department/Create
        public ActionResult Create()
        {
            using (CompanyProfileRestHelper restHelper = new CompanyProfileRestHelper())
            {
                var response = restHelper.GetCompanyProfiles();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    DepartmentViewModel branchViewModel = new DepartmentViewModel
                    {
                        CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content)
                    };
                    return View(branchViewModel);
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    return View("Index");
                }

            }
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(DepartmentViewModel departmentViewModel)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)

                {
                    using (DepartmentRestHelper restHelper = new DepartmentRestHelper())
                    {


                        var config = new MapperConfiguration(cfg => cfg.CreateMap<DepartmentViewModel, Department>());
                        var mapper = config.CreateMapper();
                        Department department = mapper.Map<Department>(departmentViewModel);

                        restHelper.AddDepartment(department);

                    }


                    this.AddNotification("Department created successfully!", NotificationType.SUCCESS);
                    return RedirectToAction("Index");
                }
                else
                {
                    this.AddNotification("Some fields are required", NotificationType.WARNING);

                    CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper();
                    var response = companyProfileRestHelper.GetCompanyProfiles();
                    if (response.StatusCode == HttpStatusCode.OK)
                        departmentViewModel.CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content);

                    return View("Create", departmentViewModel);
                }


            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Department/Edit/id
        public ActionResult Edit(int id)
        {
            using (DepartmentRestHelper restHelper = new DepartmentRestHelper())
            {
                Department department = restHelper.GetDepartmentById(id);
                var config = new MapperConfiguration(cfg => cfg.CreateMap<Department, DepartmentViewModel>());
                var mapper = config.CreateMapper();
                DepartmentViewModel departmentViewModel = mapper.Map<DepartmentViewModel>(department);
                using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
                {
                    var response = companyProfileRestHelper.GetCompanyProfiles();
                    if (response.StatusCode == HttpStatusCode.OK)
                        departmentViewModel.CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content);

                }
                return View(departmentViewModel);
            }
        }

        // POST: Department/Edit/model
        [HttpPost]
        public ActionResult Edit(DepartmentViewModel departmentViewModel)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)

                {
                    using (DepartmentRestHelper departmentRestHelper = new DepartmentRestHelper())
                    {

                        var config = new MapperConfiguration(cfg => cfg.CreateMap<DepartmentViewModel, Department>());
                        var mapper = config.CreateMapper();
                        Department department = mapper.Map<Department>(departmentViewModel);


                        var response=departmentRestHelper.UpdateDepartment(department);

                        if (response.StatusCode == HttpStatusCode.OK)
                            this.AddNotification("Department profile updated successfully!", NotificationType.SUCCESS);
                        else
                            this.AddNotification(response.Content, NotificationType.ERROR);
                    }

                   

                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                this.AddNotification("Error while update Department Profile:" + ex.Message, NotificationType.ERROR);
                return View();
            }
        }

        // POST: Department/Delete/id
        public ActionResult Delete(int id)
        {


            using (DepartmentRestHelper departmentRestHelper = new DepartmentRestHelper())
            {

                var response = departmentRestHelper.DeleteDepartmentById(id);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    this.AddNotification("Department deleted successfully!", NotificationType.SUCCESS);
                    return RedirectToAction("Index");
                }
                else
                {
                    this.AddNotification("Something went wrong..", NotificationType.ERROR);
                    return RedirectToAction("Index");
                }
            }


        }
    }
}