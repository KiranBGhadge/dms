﻿using AutoMapper;
using DMS.Extensions;
using DMS.Model;
using DMS.Models;
using DMS.RestHelpers;
using log4net;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace DMS.Controllers.Configurations
{
    public class UserController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UserController));
        // GET: User
        public ActionResult Index()
        {
            using (UserRestHelper restHelper = new UserRestHelper())
            {
                IList<Users> userProfiles = new List<Users>();
                var response = restHelper.GetAllUserProfiles();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    userProfiles = JsonConvert.DeserializeObject<List<Users>>(response.Content);
                    return View(userProfiles);
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    return View(userProfiles);
                }
            }

        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
            {
                var companyResponse = companyProfileRestHelper.GetCompanyProfiles();

                using (BranchProfileRestHelper branchProfileRestHelper = new BranchProfileRestHelper())
                {
                    var branchResponse = branchProfileRestHelper.GetBranchProfiles();

                    if (companyResponse.StatusCode == HttpStatusCode.OK && branchResponse.StatusCode == HttpStatusCode.OK)
                    {
                        UserViewModel userViewModel = new UserViewModel
                        {
                            CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(companyResponse.Content),
                            Branches = JsonConvert.DeserializeObject<List<Branch>>(branchResponse.Content)

                        };
                        return View(userViewModel);
                    }
                }


            }
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (UserRestHelper userRestHelper = new UserRestHelper())
                    {
                        var config = new MapperConfiguration(cfg => cfg.CreateMap<UserViewModel, Users>());
                        var mapper = config.CreateMapper();
                        Users users = mapper.Map<Users>(userViewModel);

                        var response = userRestHelper.AddUser(users);
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            this.AddNotification("User profile created successfully!", NotificationType.SUCCESS);
                            Logger.Info(string.Format("User profile:{0} created successfully.", users.LoginName));
                        }
                        else
                        {
                            this.AddNotification(response.Content, NotificationType.ERROR);
                            Logger.Error(string.Format("Error occured while creating User profile:{0} Exceprion:", users.LoginName, response.Content));
                        }
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
            {
                var companyResponse = companyProfileRestHelper.GetCompanyProfiles();

                using (BranchProfileRestHelper branchProfileRestHelper = new BranchProfileRestHelper())
                {
                    var branchResponse = branchProfileRestHelper.GetBranchProfiles();

                    if (companyResponse.StatusCode == HttpStatusCode.OK && branchResponse.StatusCode == HttpStatusCode.OK)
                    {
                        using (UserRestHelper userRestHelper = new UserRestHelper())
                        {
                            Users users = userRestHelper.GetUserProfileById(id);
                            var config = new MapperConfiguration(cfg => cfg.CreateMap<Users, UserViewModel>());
                            var mapper = config.CreateMapper();
                            UserViewModel userViewModel = mapper.Map<UserViewModel>(users);
                            userViewModel.CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(companyResponse.Content);
                            userViewModel.Branches = JsonConvert.DeserializeObject<List<Branch>>(branchResponse.Content);
                            return View(userViewModel);
                        }

                    }
                }


            }
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (UserRestHelper userRestHelper = new UserRestHelper())
                    {
                        var config = new MapperConfiguration(cfg => cfg.CreateMap<UserViewModel, Users>());
                        var mapper = config.CreateMapper();
                        Users users = mapper.Map<Users>(userViewModel);

                        var response = userRestHelper.UpdateUserProfile(users);
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            this.AddNotification("User profile updated successfully!", NotificationType.SUCCESS);
                            Logger.Info(string.Format("User profile:{0} updated successfully.", users.LoginName));
                        }
                        else
                        {
                            this.AddNotification(response.Content, NotificationType.ERROR);
                            Logger.Error(string.Format("Error occured while creating User profile:{0} Exceprion:", users.LoginName, response.Content));
                        }
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            using (UserRestHelper userRestHelper = new UserRestHelper())
            {
                var response = userRestHelper.DeleteUserProfileById(id);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    this.AddNotification("User profile deleted successfully!", NotificationType.SUCCESS);
                    Logger.Info("User profile deleted successfully!");
                    return RedirectToAction("Index");
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    Logger.Error("Error occured while deleting user profile:" + response.Content);
                    return RedirectToAction("Index");
                }
            }

        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
