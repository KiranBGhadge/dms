﻿using AutoMapper;
using DMS.Extensions;
using DMS.Model;
using DMS.Models;
using DMS.RestHelpers;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace DMS.Controllers.Configurations
{
    public class BranchController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(BranchController));
        // GET: Branch
        public ActionResult Index()
        {
            using (BranchProfileRestHelper restHelper = new BranchProfileRestHelper())
            {
                IList<Branch> branchProfiles = new List<Branch>();
                var response = restHelper.GetBranchProfiles();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    branchProfiles = JsonConvert.DeserializeObject<List<Branch>>(response.Content);
                    return View(branchProfiles);
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    return View(branchProfiles);
                }
            }

        }


        // GET: Branch/Create
        public ActionResult Create()
        {
            using (CompanyProfileRestHelper restHelper = new CompanyProfileRestHelper())
            {
                var response = restHelper.GetCompanyProfiles();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    BranchViewModel branchViewModel = new BranchViewModel
                    {
                        CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content)
                    };
                    return View(branchViewModel);
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    return View("Index");
                }
               
            }
        }

        // POST: Branch/Create
        [HttpPost]
        public ActionResult Create(BranchViewModel branchViewModel)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)

                {
                    using (BranchProfileRestHelper restHelper = new BranchProfileRestHelper())
                    {


                        var config = new MapperConfiguration(cfg => cfg.CreateMap<BranchViewModel, Branch>());
                        var mapper = config.CreateMapper();
                        Branch branch = mapper.Map<Branch>(branchViewModel);

                        var response= restHelper.AddBranchProfile(branch);

                        if(response.StatusCode==HttpStatusCode.OK)
                        {
                            this.AddNotification("Branch profile created successfully!", NotificationType.SUCCESS);
                            Logger.Info(string.Format("Branch profile:{0} created successfully.",branch.Name));
                        }
                        else
                        {
                            this.AddNotification(response.Content, NotificationType.ERROR);
                            Logger.Error(string.Format("Error occured while creating Branch profile:{0} Exceprion:", branch.Name,response.Content));
                        }

                    }

                   
                    return RedirectToAction("Index");
                }
                else
                {
                    this.AddNotification("Some fields are required", NotificationType.WARNING);

                    CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper();
                    var response= companyProfileRestHelper.GetCompanyProfiles();
                    if (response.StatusCode == HttpStatusCode.OK)
                        branchViewModel.CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content);

                    return View("Create", branchViewModel);
                }


            }
            catch (Exception)
            {
                return View();
            }
        }        // GET: Branch/Edit/5
        public ActionResult Edit(int id)
        {
            using (BranchProfileRestHelper restHelper = new BranchProfileRestHelper())
            {
                Branch branch = restHelper.GetBranchProfileById(id);
                var config = new MapperConfiguration(cfg => cfg.CreateMap<Branch, BranchViewModel>());
                var mapper = config.CreateMapper();
                BranchViewModel branchViewModel = mapper.Map<BranchViewModel>(branch);
                using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
                {
                    var response= companyProfileRestHelper.GetCompanyProfiles();
                    if(response.StatusCode==HttpStatusCode.OK)
                    branchViewModel.CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content);

                }
                return View(branchViewModel);
            }
        }

        // POST: Branch/Edit/5
        [HttpPost]
        public ActionResult Edit(BranchViewModel branchViewModel)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)

                {
                    using (BranchProfileRestHelper branchProfileRestHelper = new BranchProfileRestHelper())
                    {
                        var config = new MapperConfiguration(cfg => cfg.CreateMap<BranchViewModel, Branch>());
                        var mapper = config.CreateMapper();
                        Branch branch = mapper.Map<Branch>(branchViewModel);

                        var response= branchProfileRestHelper.UpdateBranchProfile(branch);
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            this.AddNotification("Branch profile updated successfully!", NotificationType.SUCCESS);
                            Logger.Info(string.Format("Branch profile:{0} updated successfully.",branch.Name));
                        }
                        else
                        {
                            this.AddNotification(response.Content, NotificationType.ERROR);
                            Logger.Error(string.Format("Error occured while updating branch:{0} Exception:",branch.Name,response.Content));
                        }
                    }                    

                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                this.AddNotification("Error while update Branch Profile:" + ex.Message, NotificationType.ERROR);
                Logger.Error("Error while update Branch Profile:" + ex.Message);
                return View();
            }
        }


        public ActionResult Delete(int id)
        {


            using (BranchProfileRestHelper branchProfileRestHelper = new BranchProfileRestHelper())
            {
                var response = branchProfileRestHelper.DeleteBranchProfileById(id);
                if (response.StatusCode==HttpStatusCode.OK)
                {
                    this.AddNotification("Branch profile deleted successfully!", NotificationType.SUCCESS);
                    Logger.Info("Branch profile deleted successfully!");
                    return RedirectToAction("Index");
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    Logger.Error("Error occured while deleting branch profile:" + response.Content);
                    return RedirectToAction("Index");
                }
            }


        }
    }
}
