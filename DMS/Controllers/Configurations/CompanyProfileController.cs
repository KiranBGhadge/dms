﻿using AutoMapper;
using DMS.Extensions;
using DMS.Model;
using DMS.Models;
using DMS.RestHelpers;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DMS.Controllers.Configurations
{
    public class CompanyProfileController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CompanyProfileController));
        public CompanyProfileController()
        {

        }
        // GET: CompanyProfile
        public ActionResult Index()
        {
            using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
            {
                List<CompanyProfile> companyProfiles = new List<CompanyProfile>();
                var response = companyProfileRestHelper.GetCompanyProfiles();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    companyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content);
                }
                return View(companyProfiles);
            }
        }

        public ActionResult Create()
        {
            using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
            {
                
                CompanyProfileViewModel companyviewModel = new CompanyProfileViewModel();
                var response = companyProfileRestHelper.GetCompanyProfiles();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    companyviewModel.CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content);
                    return View(companyviewModel);
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    return View("Index");
                }
                
            }
        }

        [HttpPost]
        public ActionResult Create(CompanyProfileViewModel _model, HttpPostedFileBase file)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)

                {
                    using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
                    {
                        var config = new MapperConfiguration(cfg => cfg.CreateMap<CompanyProfileViewModel, CompanyProfile>());
                        var mapper = config.CreateMapper();
                        CompanyProfile companyProfile = mapper.Map<CompanyProfile>(_model);
                        if (file != null)
                        {
                            var fileName = Path.GetFileName(file.FileName);
                            var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                            var filePath = "~/App_Data/uploads/" + fileName;
                            companyProfile.CompanyLogoPath = filePath;
                            file.SaveAs(path);
                        }
                        var response = companyProfileRestHelper.AddCompanyProfile(companyProfile);
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            this.AddNotification("Company profile created successfully!", NotificationType.SUCCESS);
                            Logger.Info(string.Format("Company {0} created successfully.", companyProfile.Name));
                        }
                        else
                        {
                            this.AddNotification(response.Content, NotificationType.ERROR);
                            Logger.Error("Error occured while creating company profile:" + response.Content);
                        }
                    }



                    return RedirectToAction("Index");
                }
                else
                {
                    this.AddNotification("Some fields are required", NotificationType.WARNING);
                    using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
                    {
                        var response= companyProfileRestHelper.GetCompanyProfiles();
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            _model.CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content);
                            
                        }
                       
                    }
                    return View("Create", _model);
                }


            }
            catch (Exception)
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
            {
                CompanyProfile companyProfile = companyProfileRestHelper.GetCompanyProfileById(id);
                var config = new MapperConfiguration(cfg => cfg.CreateMap<CompanyProfile, CompanyProfileViewModel>());
                var mapper = config.CreateMapper();
                CompanyProfileViewModel companyProfileViewModel = mapper.Map<CompanyProfileViewModel>(companyProfile);

                var response = companyProfileRestHelper.GetCompanyProfiles();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    companyProfileViewModel.CompanyProfiles = JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content);
                    return View(companyProfileViewModel);
                }
                else
                {
                    this.AddNotification(response.Content, NotificationType.ERROR);
                    return View("Index");
                }
              
            }
        }

        [HttpPost]
        public ActionResult Edit(CompanyProfileViewModel companyProfileViewModel, HttpPostedFileBase file)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)

                {
                    using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
                    {
                        string filePath = string.Empty;
                        if (file != null)
                        {
                            var fileName = Path.GetFileName(file.FileName);
                            var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                            filePath = "~/App_Data/uploads/" + fileName;
                            file.SaveAs(path);
                        }
                        var config = new MapperConfiguration(cfg => cfg.CreateMap<CompanyProfileViewModel, CompanyProfile>());
                        var mapper = config.CreateMapper();
                        CompanyProfile companyProfile = mapper.Map<CompanyProfile>(companyProfileViewModel);
                        companyProfile.CompanyLogoPath = file != null ? filePath : companyProfileViewModel.CompanyLogoPath;

                        var response = companyProfileRestHelper.UpdateCompanyProfile(companyProfile);
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            this.AddNotification("Company profile updated successfully!", NotificationType.SUCCESS);
                            Logger.Info(string.Format("Company profile:{0} updated successfully!",companyProfile.Name));
                        }
                        else
                        {
                            this.AddNotification(response.Content, NotificationType.ERROR);
                            Logger.Info(string.Format("Error occured while updating company profile:{0} Exception:",companyProfile.Name,response.Content));
                        }
                    }



                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                this.AddNotification("Error while update company Profile:" + ex.Message, NotificationType.ERROR);
                Logger.Info(string.Format("Error occured while updating company profile Exception:{0}", ex.Message));
                return View();
            }
        }


        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add update logic here
                using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
                {
                    var response = companyProfileRestHelper.DeleteCompanyProfileById(id);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        this.AddNotification("Company profile deleted successfully!", NotificationType.SUCCESS);
                        Logger.Info(string.Format("Company profile deleted successfully."));
                    }
                    else
                    {
                        this.AddNotification(response.Content, NotificationType.ERROR);
                        Logger.Error("Error occured while deleting company profile:" + response.Content);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }

        }
    }
}