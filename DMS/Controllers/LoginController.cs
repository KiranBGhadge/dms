﻿using System.Web.Mvc;
using DMS.RestHelpers;
using DMS.Models;
using Newtonsoft.Json;
using DMS.Model;
using System.Collections.Generic;
using DMS.Extensions;
using System.Web.Security;

namespace DMS.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            using (CompanyProfileRestHelper restHelper=new CompanyProfileRestHelper())
            {
                var response = restHelper.GetCompanyProfiles();
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    LoginViewModel loginViewModel = new LoginViewModel()
                    {
                        CompanyProfiles= JsonConvert.DeserializeObject<List<CompanyProfile>>(response.Content)
                    };

                    return View(loginViewModel);
                }
                else
                {
                    return View();
                }
            }   
           
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            using (UserRestHelper userRestHelper=new UserRestHelper())
            {
                var userResponse = userRestHelper.GetUserProfileByName(loginViewModel.LoginName);
                if (loginViewModel.LoginName!=null&&loginViewModel.LoginPassword!=null)
                {
                    if (loginViewModel.LoginName == userResponse.LoginName && loginViewModel.LoginPassword == userResponse.LoginPassword)
                    {
                        FormsAuthentication.SetAuthCookie(loginViewModel.LoginName, false);
                        return RedirectToActionPermanent("Index", "Home");
                    }
                    else
                    {
                        this.AddNotification("Invid LoginName/Password", NotificationType.WARNING);
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            this.AddNotification($"Logged out successfully!", NotificationType.INFO);
            return RedirectToActionPermanent("Login", "Login");
        }
    }
}