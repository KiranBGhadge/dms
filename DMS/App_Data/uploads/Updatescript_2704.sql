USE [ddms]
GO
/****** Object:  StoredProcedure [dbo].[Branch_GetAll]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`localhost`.
*/

Create PROCEDURE [dbo].[Branch_GetAll]  
  
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         Branch.Id, 
         Branch.Name, 
         Branch.DisplayName, 
		 Branch.Address,
		 Branch.CompanyId,
		 Branch.Branchcode,
		 Branch.Email,
		 Branch.WebAddress,
		 Branch.Email,
         Branch.CreatorId, 
         Branch.CreateDate, 
         Branch.UpdatorId, 
         Branch.UpdateDate
         
      FROM dbo.Branch
     

   END




GO
/****** Object:  StoredProcedure [dbo].[Branch_GetByBranchcode]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`localhost`.
*/

Create PROCEDURE [dbo].[Branch_GetByBranchcode]  
  @Branchcode varchar(500)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         Branch.Id, 
         Branch.Name, 
         Branch.DisplayName, 
		 Branch.Address,
		 Branch.CompanyId,
		 Branch.Branchcode,
		 Branch.Email,
		 Branch.WebAddress,
		 Branch.Email,
         Branch.CreatorId, 
         Branch.CreateDate, 
         Branch.UpdatorId, 
         Branch.UpdateDate
         
      FROM dbo.Branch
     where Branch.Branchcode=@Branchcode;

   END




GO
/****** Object:  StoredProcedure [dbo].[Branch_GetByCompanyId]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`localhost`.
*/

Create PROCEDURE [dbo].[Branch_GetByCompanyId]  
  @CompanyId int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         Branch.Id, 
         Branch.Name, 
         Branch.DisplayName, 
		 Branch.Address,
		 Branch.CompanyId,
		 Branch.Branchcode,
		 Branch.Email,
		 Branch.WebAddress,
		 Branch.Email,
         Branch.CreatorId, 
         Branch.CreateDate, 
         Branch.UpdatorId, 
         Branch.UpdateDate
         
      FROM dbo.Branch
     where Branch.CompanyId=@CompanyId;

   END




GO
/****** Object:  StoredProcedure [dbo].[Branch_GetById]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`localhost`.
*/

Create PROCEDURE [dbo].[Branch_GetById]  
   @iId int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         Branch.Id, 
         Branch.Name, 
         Branch.DisplayName, 
		 Branch.Address,
		 Branch.CompanyId,
		 Branch.Branchcode,
		 Branch.Email,
		 Branch.WebAddress,
		 Branch.Email,
         Branch.CreatorId, 
         Branch.CreateDate, 
         Branch.UpdatorId, 
         Branch.UpdateDate
         
      FROM dbo.Branch
      WHERE Branch.Id = @iId

   END




GO
/****** Object:  StoredProcedure [dbo].[Branch_GetCompanyId]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`localhost`.
*/

Create PROCEDURE [dbo].[Branch_GetCompanyId]  
  @CompanyId int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         Branch.Id, 
         Branch.Name, 
         Branch.DisplayName, 
		 Branch.Address,
		 Branch.CompanyId,
		 Branch.Branchcode,
		 Branch.Email,
		 Branch.WebAddress,
		 Branch.Email,
         Branch.CreatorId, 
         Branch.CreateDate, 
         Branch.UpdatorId, 
         Branch.UpdateDate
         
      FROM dbo.Branch
     where Branch.CompanyId=@CompanyId;

   END




GO
/****** Object:  StoredProcedure [dbo].[CompanyProfile_GetAll]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`localhost`.
*/



CREATE PROCEDURE [dbo].[User_Create]  
   @iLoginName nvarchar(200),
   @iLoginPassword nvarchar(200),
   @iCompanyId int,
   @iBranchId int,
   @iRole int,
   @iLastLoginDate datetime2(0),
   @iLastLoginIP nvarchar(100),
   @iLastAlertId int,
   @iLastPasswordChanged datetime2(0),
   @iIsReset bit,
   @iIsActive bit,
   @iloginType nvarchar(150),
   @iCreatorId int,
   @iSessionTimeout int,
   @iAccessrole varchar(100)

AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      

      INSERT dbo.users(
         dbo.users.LoginName, 
         dbo.users.LoginPassword, 
         dbo.users.CompanyId, 
		 dbo.users.BranchId,
         dbo.users.Role, 
         dbo.users.LastLoginDate, 
         dbo.users.LastLoginIP, 
         dbo.users.LastAlertId, 
         dbo.users.LastPasswordChanged, 
         dbo.users.IsReset, 
         dbo.users.IsActive,        
         dbo.users.LoginType, 
         dbo.users.CreatorId, 
         dbo.users.CreateDate, 
         dbo.users.UpdatorId, 
         dbo.users.UpdateDate, 
         dbo.users.SessionTimeout,
		 dbo.users.Accessrole)
         VALUES (
            @iLoginName, 
            @iLoginPassword, 
            @iCompanyId, 
			@iBranchId,
            @iRole, 
            @iLastLoginDate, 
            @iLastLoginIP, 
            @iLastAlertId, 
            @iLastPasswordChanged, 
            @iIsReset, 
            1, 
            @iloginType, 
            @iCreatorId, 
            getdate(), 
            @iCreatorId, 
            getdate(), 
            @iAccessrole,
			@iAccessrole)

      SELECT 
         users.Id, 
         users.LoginName, 
         users.LoginPassword, 
         users.CompanyId, 
		 users.BranchId,
         users.Role, 
         users.LastLoginDate, 
         users.LastLoginIP, 
         users.LastAlertId, 
         users.LastPasswordChanged, 
         users.IsReset, 
         users.IsActive, 
         users.LoginType, 
         users.CreatorId, 
         users.CreateDate, 
         users.UpdatorId, 
         users.UpdateDate, 
         users.SessionId, 
         users.SessionTimeout,
		users.Accessrole
      FROM dbo.users
      WHERE users.Id = 
         (
            /*
            *   SSMA warning messages:
            *   M2SS0240: The behaviour of Standard Function SCOPE_IDENTITY may not be same as in MySQL
            */

            SELECT scope_identity()
         )

    

	   END




GO
/****** Object:  StoredProcedure [dbo].[User_Update]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`localhost`.
*/

CREATE PROCEDURE [dbo].[User_Update]  
   @iId int,
   @iLoginName nvarchar(50),
   @iLoginPassword nvarchar(200),
   @iCompanyId int,
   @iBranchId int,
   @iRole nvarchar(150),
   @iInfraObjectAllFlag bit,
   @iloginType nvarchar(150),
   @iUpdatorId int,
   @iSessionTimeout int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

     
      UPDATE dbo.users
         SET 
            LoginName = @iLoginName, 
            LoginPassword = @iLoginPassword, 
            CompanyId = @iCompanyId, 
			BranchId=@iBranchId,
            Role = @iRole, 
            LoginType = dbo.norm_enum$users$LoginType(@iloginType), 
            UpdatorId = @iUpdatorId, 
            UpdateDate = getdate(), 
            SessionTimeout = @iSessionTimeout
      WHERE users.Id = @iId

      SELECT 
         users.Id, 
         users.LoginName, 
         users.LoginPassword, 
         users.CompanyId, 
		 users.BranchId,
         users.Role, 
         users.LastLoginDate, 
         users.LastLoginIP, 
         users.LastAlertId, 
         users.LastPasswordChanged, 
         users.IsReset, 
         users.IsActive, 
         users.LoginType, 
         users.CreatorId, 
         users.CreateDate, 
         users.UpdatorId, 
         users.UpdateDate, 
         users.SessionId, 
         users.SessionTimeout,
		  users.AccessRole
      FROM dbo.users
      WHERE users.Id = @iId

     
   END




GO
/****** Object:  StoredProcedure [dbo].[UserRole_GetAll]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`localhost`.
*/

Create PROCEDURE [dbo].[UserRole_GetAll]  
  @Branchcode varchar(500)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SELECT 
         UserRole.Id, 
         UserRole.Role, 
         UserRole.Access
		 from UserRole;
		 
		 END 
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Branch](
	[Id] [bigint] IDENTITY(89,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[DisplayName] [varchar](45) NOT NULL,
	[Branchcode] [nvarchar](150) NULL,
	[CompanyId] [bigint] NULL,
	[Address] [varchar](150) NOT NULL,
	[Phone] [varchar](150) NOT NULL,
	[Fax] [varchar](150) NOT NULL,
	[WebAddress] [varchar](150) NOT NULL,
	[Email] [varchar](150) NOT NULL,
	[IsActive] [smallint] NOT NULL,
	[CreatorId] [bigint] NOT NULL,
	[CreateDate] [datetime2](0) NOT NULL,
	[UpdatorId] [bigint] NOT NULL,
	[UpdateDate] [datetime2](0) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [bigint] NULL,
	[Role] [nvarchar](200) NOT NULL,
	[Access] [nvarchar](500) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users]    Script Date: 4/27/2020 4:09:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[Id] [bigint] IDENTITY(336,1) NOT NULL,
	[LoginName] [varchar](200) NOT NULL,
	[LoginPassword] [varchar](45) NOT NULL,
	[CompanyId] [bigint] NOT NULL,
	[BranchId] [bigint] NULL,
	[Role] [bigint] NULL,
	[LastLoginDate] [datetime2](0) NULL,
	[LastLoginIP] [varchar](45) NULL,
	[LastAlertId] [int] NULL,
	[LastEventId] [bigint] NULL,
	[LastPasswordChanged] [datetime2](0) NULL,
	[IsReset] [smallint] NULL,
	[IsActive] [smallint] NULL,
	[LoginType] [varchar](18) NULL,
	[CreatorId] [bigint] NULL,
	[CreateDate] [datetime2](0) NULL,
	[UpdatorId] [bigint] NULL,
	[UpdateDate] [datetime2](0) NULL,
	[SessionId] [varchar](500) NULL,
	[SessionTimeout] [bigint] NULL,
	[IsProperLoggedOut] [smallint] NULL,
	[LastLoggedOutDate] [datetime2](7) NULL,
	[AccessRole] [varchar](45) NULL,
 CONSTRAINT [PK_users_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__Role__0D7A0286]  DEFAULT (NULL) FOR [Role]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__LastLogin__0E6E26BF]  DEFAULT (NULL) FOR [LastLoginDate]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__LastLogin__0F624AF8]  DEFAULT (NULL) FOR [LastLoginIP]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__LastAlert__10566F31]  DEFAULT (NULL) FOR [LastAlertId]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__LastIncid__114A936A]  DEFAULT (NULL) FOR [LastEventId]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__LastPassw__123EB7A3]  DEFAULT (NULL) FOR [LastPasswordChanged]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__IsReset__1332DBDC]  DEFAULT (NULL) FOR [IsReset]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__IsActive__14270015]  DEFAULT (NULL) FOR [IsActive]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__LoginType__17036CC0]  DEFAULT (NULL) FOR [LoginType]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__CreatorId__17F790F9]  DEFAULT (NULL) FOR [CreatorId]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__CreateDat__18EBB532]  DEFAULT (NULL) FOR [CreateDate]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__UpdatorId__19DFD96B]  DEFAULT (NULL) FOR [UpdatorId]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__UpdateDat__1AD3FDA4]  DEFAULT (NULL) FOR [UpdateDate]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__SessionId__1BC821DD]  DEFAULT (NULL) FOR [SessionId]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF__users__SessionTi__1CBC4616]  DEFAULT (NULL) FOR [SessionTimeout]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_company_profile] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[company_profile] ([Id])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_company_profile]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ddms_master.CompanyProfile_GetAll' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'CompanyProfile_GetAll'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ddms_master.CompanyProfile_GetById' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'CompanyProfile_GetById'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ddms_master.User_Create' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'User_Create'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ddms_master.User_Update' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'User_Update'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ddms_master.users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users'
GO
