GEMINI 6.9.2
============

What's new and changed: http://gemini.countersoft.com/workspace/801/app/changelog/view


You have two options for installation:

1. Automated Installation
-------------------------

Run the installer to configure the Gemini web app and SQL database.

2. Manual Installation
----------------------

Manually configure your web server and SQL database.

Requirements
------------
- Microsoft .NET Framework v4.7.2
- Microsoft SQL Server 2005/2008/Express (case-insensitive)
- Microsoft IIS Web Server

Change Log
----------
See http://gemini.countersoft.com

Documentation
-------------
http://docs.countersoft.com

Help Desk
-------------
http://helpdesk.countersoft.com


