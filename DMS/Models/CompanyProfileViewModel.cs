﻿using DMS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class CompanyProfileViewModel
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Name")]
        [Required(ErrorMessage="Please enter name"),MaxLength(130)]
        public string Name { get; set; }

        [JsonProperty("DisplayName")]
        [Display(Name = "Display Name")]
        [Required(ErrorMessage = "Please enter Display Name"), MaxLength(30)]
        public string DisplayName { get; set; }

        [JsonProperty("IsParent")]
        public bool IsParent { get; set; }

        
        [JsonProperty("ParentId")]
        public long ParentId { get; set; }

        [Display(Name = "Company Logo")]
        [JsonProperty("CompanyLogoPath")]
        public string CompanyLogoPath { get; set; }

        [JsonProperty("CompanyId")]
        public string CompanyId { get; set; }

        [JsonProperty("Address")]
        public string Address { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Fax")]
        public string Fax { get; set; }

        [JsonProperty("WebAddress")]
        [Display(Name = "Web Address")]
        public string WebAddress { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Creatorid")]
        public long Creatorid { get; set; }

        [JsonProperty("UpdatorId")]
        public long UpdatorId { get; set; }

        public IList<CompanyProfile> CompanyProfiles { get; set; }
    }
}