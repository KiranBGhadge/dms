﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMS.Model;
namespace DMS.Models
{
    public class LoginViewModel
    {
        public string LoginName { get; set; }
        public string LoginPassword { get; set; }
        public int CompanyId { get; set; }
        public List<CompanyProfile> CompanyProfiles { get; set; }
    }
}