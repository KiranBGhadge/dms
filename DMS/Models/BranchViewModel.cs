﻿using DMS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class BranchViewModel
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter Branch name"), MaxLength(130)]
        [JsonProperty("Name")]
        public string Name { get; set; }
        [Display(Name="Display Name")]
        [JsonProperty("DisplayName")]
        public string DisplayName { get; set; }
        [Required(ErrorMessage = "Please enter Branch code"), MaxLength(130)]
        [Display(Name = "Branch Code")]
        [JsonProperty("Branchcode")]
        public string Branchcode { get; set; }

        [Required(ErrorMessage = "Please select company")]
        [JsonProperty("CompanyId")]
        public int CompanyId { get; set; }
        [JsonProperty("Address")]
        public string Address { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
        [JsonProperty("Fax")]
        public string Fax { get; set; }
        [JsonProperty("WebAddress")]
        public string WebAddress { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("Creatorid")]
        public int Creatorid { get; set; }
        [JsonProperty("CreateDate")]
        public DateTime CreateDate { get; set; }
        [JsonProperty("UpdatorId")]
        public int UpdatorId { get; set; }
        [JsonProperty("UpdateDate")]
        public DateTime UpdateDate { get; set; }
        public IList<CompanyProfile> CompanyProfiles { get; set; }
    }
}