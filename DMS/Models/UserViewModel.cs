﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DMS.Model;
using Newtonsoft.Json;

namespace DMS.Models
{
    public class UserViewModel
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("LoginName")]
        [Display(Name = "Login Name")]
        public string LoginName { get; set; }

        [JsonProperty("LoginPassword")]
        [Display(Name = "Login Password")]
        public string LoginPassword { get; set; }

        [JsonProperty("Company")]
        [Display(Name = "Company")]
        public int CompanyId { get; set; }

        [JsonProperty("BranchId")]
        [Display(Name = "Branch")]
        public int BranchId { get; set; }

        [JsonProperty("UserName")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Role")]
        public UserRole Role { get; set; }

        [JsonProperty("LastLoginDate")]
        public DateTime LastLoginDate { get; set; }

        [JsonProperty("LastLoginIP")]
        public string LastLoginIP { get; set; }

        [JsonProperty("LastAlertId")]
        public int LastAlertId { get; set; }

        [JsonProperty("LastEventId")]
        public int LastEventId { get; set; }

        [JsonProperty("LastPasswordChanged")]
        public DateTime LastPasswordChanged { get; set; }

        [JsonProperty("IsReset")]
        public int IsReset { get; set; }

        [JsonProperty("IsActive")]
        public int IsActive { get; set; }

        [JsonProperty("CreatorId")]
        public int CreatorId { get; set; }

        [JsonProperty("CreateDate")]
        public DateTime CreateDate { get; set; }

        [JsonProperty("UpdatorId")]
        public int UpdatorId { get; set; }

        [JsonProperty("UpdateDate")]
        public DateTime UpdateDate { get; set; }

        [JsonProperty("SessionId")]
        public int SessionId { get; set; }

        [JsonProperty("SessionTimeout")]
        public string SessionTimeout { get; set; }

        [JsonProperty("IsProperLoggedOut")]
        public int IsProperLoggedOut { get; set; }

        [JsonProperty("LastLoggedOutDate")]
        public DateTime LastLoggedOutDate { get; set; }

        [JsonProperty("AccessRole")]
        public string AccessRole { get; set; }

        public List<Branch> Branches { get; set; }
        public List<CompanyProfile> CompanyProfiles { get; set; }
    }
    public enum UserRole : int
    {
        SuperAdmin = 0,
        PhoenixAdmin = 1,
        OfficeAdmin = 2,
        ReportUser = 3,
        BillingUser = 4
    }
}