﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using RestSharp;
using DMS.Model;
using DMS.RestHelpers;
namespace DMS.Models
{
    public class DepartmentViewModel
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter Department name"), MaxLength(130)]
        [JsonProperty("Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please select company")]
        [JsonProperty("CompanyId")]
        public int CompanyId { get; set; }

        [JsonProperty("CreatorId")]
        public int CreatorId { get; set; }
        [JsonProperty("CreateDate")]
        public DateTime CreateDate { get; set; }
        [JsonProperty("UpdatorId")]
        public int UpdatorId { get; set; }
        [JsonProperty("UpdateDate")]
        public DateTime UpdateDate { get; set; }
        public IList<CompanyProfile> CompanyProfiles { get; set; }
      
        public string GetCompanyNameByCompanyId(int companyId)
        {
            using (CompanyProfileRestHelper companyProfileRestHelper = new CompanyProfileRestHelper())
            {
                var company = companyProfileRestHelper.GetCompanyProfileById(CompanyId);
                return company.Name;
            }
        }
    }
}